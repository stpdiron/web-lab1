<?php

$data = file("data.csv");
$rows = array();

foreach ($data as $line)
{
    $rows[] = explode(",", $line);
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <title>Лабораторная работа 1</title>
    <style type="text/css">
        
        /* by type */
        h2 { 
         font-size: 18px; 
         font-weight: 600;
         margin: 0px;
         padding: 0px 0px 3px 0px;
        }

        h3 { 
         font-size: 16px;
         margin: 0px;
         padding: 0px 0px 3px 0px;
         font-weight: 400;
        }

        .main-form fieldset {
            display: flex;
            justify-content: start;
            flex-wrap: wrap;
            line-height: 2;
            border-radius: 4px;
            background-color: none;
            border: none;
            margin-inline-start: 0px;
            margin-inline-end: 0px;
            padding-inline-start: 0px;
            padding-inline-end: 0px;
            /* border: 1px solid #202236; */
        }

        /* by class */
        .dark-body {
            font-family: 'Roboto', sans-serif;
            font-size: 14px;
            padding: 0px 0px 50px 0px;
            margin: 0px;
            background-color: #202236;
            color: #ebedf2;
        }

        .main-form {
            width: 30vw;
            display: flex;
            flex-direction: column;
            justify-content: space-evenly;
        }

        @media (max-width: 1000px) {
            .main-form {
                width: 100%;
                margin-bottom: 20px;
            }
        }

        .main-form select {
            cursor: pointer;
        }

        .main-form input[type=text], select {
            display: block;
            width: 100%;
            height: 45px;
            padding-left: 10px;
            padding-right: 10px;
            color: #ebedf2;
            background-color: #202236;
            outline: none;
            border: none;
            border-radius: 4px;
            -moz-box-shadow: 2px;
            -webkit-box-shadow: 2px;
            box-shadow: 2px;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }

        .main-form input[type=text]:focus, select:focus {
            outline: #1890ff solid 2px;
        }

        .main-form button {
            margin-top: 15px;
            cursor: pointer;
            background-color:  #1890ff;
            border-radius: 4px;
            border: none;
            color: #ffffff;
            width: 100%;
            height: 50px;
            text-align: center;
            text-decoration: none;
            font-size: 16px;
        }

        .main-form button:hover {
            background-color:  #369eff;
        }

        .main-form input[type=radio] {
            margin-right: 5px;
        }

        fieldset.radios div {
            margin-right: 20px;
            position: relative;
        }

        .main-form label:before {
            content: "";
            cursor: pointer;
            box-sizing: inherit;
            display: inline-block;
            position: absolute;
            height: 20px;
            width: 20px;
            top: 0;
            left: 0;
            background-color: #202236;
            border: none;
            border-radius: 20px;
        }

        .main-form input[type=radio]:checked~label:before {
            background-color: #ffffff;
            border: 7px solid #1890ff;
            box-sizing: border-box;
        }

        .main-form input[type=radio] {
            visibility: hidden;
        }

        @media (min-width: 1000px) {
            .content-block {
                margin-left: 15%;
                margin-right: 15%;
                margin-bottom: 30px;
                padding: 15px 3vw;
                background-color: #2D2F47;
                border-radius: 4px;
            }
        }

        @media (max-width: 1000px) {
                .content-block {
                    display: flexbox;
                    margin-left: 5%;
                    margin-right: 5%;
                    flex-direction: column;
                    margin-bottom: 30px;
                    padding: 15px 3vw;
                    background-color: #2D2F47;
                    border-radius: 4px;
            }   
        }

        .colored-text {
            color: #1890ff;
        }

        .inlined {
            display: flex;
            justify-content: space-evenly;
        }

        /* by id */
        #result-table {
            display: block;
            overflow-x: auto;
            white-space: nowrap;
            width: 100%;
            border-collapse: collapse;
            border: 2px solid #3f4157;
            font-size: 15px;
            margin: 20px 0 20px 0;
        }

        #result-table tbody, thead{
            display: table;
            width: 100%;
        }

        #result-table td {
            padding: 15px 15px;
        }


        #coordinates-canvas {
            padding: 10px;
            height: min-content;
        }

       </style>
</head>
<body class="dark-body">
    
    <header class="content-block">
        <h2>Ненов Владислав</h2>
        <h3 class="colored-text">Группа 32082, Вариант 1607</h3>
    </header>

    <div class="content-block bordered inlined">
        <canvas id="coordinates-canvas" class="bordered"></canvas>
        <form class="main-form" action="script.php" method="GET" onsubmit="return validateOnSubmit()">
            <fieldset class="radios">
                <legend>Координата X:</legend>
                <div>
                    <input type="radio" id="-2-radio" value="-2" name="x-coordinate" checked>      <label for="-2-radio">-2</label>
                </div>
                <div>
                    <input type="radio" id="-1.5-radio" value="-1.5" name="x-coordinate">    <label for="-1.5-radio">-1.5</label>
                </div>
                <div>
                    <input type="radio" id="-1-radio" value="-1" name="x-coordinate">      <label for="-1-radio">-1</label>
                </div>
                <div>
                    <input type="radio" id="-0.5-radio" value="-0.5" name="x-coordinate">    <label for="-0.5-radio">-0.5</label>
                </div>
                <div>
                    <input type="radio" id="0-radio" value="0" name="x-coordinate">       <label for="0-radio">0</label>
                </div>
                <div>
                    <input type="radio" id="0.5-radio" value="0.5" name="x-coordinate">     <label for="0.5-radio">0.5</label>
                </div>
                <div>
                    <input type="radio" id="1-radio" value="1" name="x-coordinate">       <label for="1-radio">1</label>
                </div>
                <div>
                    <input type="radio" id="1.5-radio" value="1.5" name="x-coordinate">     <label for="1.5-radio">1.5</label>
                </div>
                <div>
                    <input type="radio" id="2-radio" value="2" name="x-coordinate">       <label for="2-radio">2</label>
                </div>
            </fieldset>
            
            <fieldset>
                <legend>Координата Y:</legend>
                <input type="text" id="y-coordinate" name="y-coordinate" value="1">
            </fieldset>
            
            <fieldset>
                <legend>Значение R:</legend>
                <select name="r-value" id="r-value" required >
                    <option value="1" selected>1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="3">4</option>
                    <option value="3">5</option>
                  </select>
            </fieldset>

            <button id="send-btn">Проверить</button>

        </form>
    </div>

    <div class="content-block bordered">
        <table id="result-table" border="2">
            <tbody>
                <tr>
                    <td><b>Время старта</b></td>
                    <td><b>Время выполнения (c)</b></td>
                    <td><b>Координата X</b></td>
                    <td><b>Координата Y</b></td>
                    <td><b>Значение R</b></td>
                    <td><b>Попадание</b></td>
                </tr>

                <?php foreach($rows as $row): ?>
                    <tr>
                        <td> <?php echo $row[0] ?> </td>
                        <td> <?php echo $row[1] ?> </td>
                        <td class="x-cell"> <?php echo $row[2] ?> </td>
                        <td class="y-cell"> <?php echo $row[3] ?> </td>
                        <td class="r-cell"> <?php echo $row[4] ?> </td>
                        <td class="colored-text hit-cell"><b><?php echo $row[5] ?> </b></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <script src="draw.js"></script>
    <script src="validate.js"></script>
</body>
</html>