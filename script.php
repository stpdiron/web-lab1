<?php

// add r validation
function validate($x, $y, $r) {
    if (!is_numeric($x) || !is_numeric($y) || !is_numeric($r))
        return false;
    $r = floatval($r);
    $x = floatval($x);
    $y = floatval($y);
    if ($y < -5 || $y > 5 || $x < -2 || $x > 2 || $r < 1 || $r > 5) {
        return false;
    }
    return floatval($r) >= 0;
}

function process($x, $y, $r) {
    $x = floatval($x);
    $y = floatval($y);
    $r = floatval($r);
    return (
        ($x<=0 && $y>=0 && pow($x,2)+pow($y,2)<=pow($r/2, 2)) ||
        ($x>=0 && $y>=0 && $x<=$r/2 && $y<=$r) ||
        ($x>=0 && $y<=0 && $y>=$x/2-$r/2)
    );
}

function write_result($data_arr) {
    file_put_contents("data.csv", implode(",", $data_arr) . "\n", FILE_APPEND | LOCK_EX);
}

$time_start = date_timestamp_get(date_create());
$x = $_GET["x-coordinate"];
$y = $_GET["y-coordinate"];
$r = $_GET["r-value"];
$refer = '';

if ($_SERVER['HTTP_REFERER'] != null) {
    $refer = $_SERVER['HTTP_REFERER'];
    // $refer = preg_replace("/\/\?.*$/", "", $refer);
}


if (validate($x, $y, $r)) {
    $x = round($x, 5);
    $y = round($y, 5);
    $r = round($r, 5);
    $result = process($x, $y, $r);
    $work_time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
    write_result(array(date("Y-m-d H:i:s", $time_start), $work_time, $x, $y, $r, $result ? "+" : "-"));
    $resstr = $result ? 'true' : 'false';
    header("Location: $refer");
} else {
    echo "<script> alert('Некорректные данные (x={$x} y={$y} r={$r})'); 
    window.location.href = '{$refer}'; </script>"; 
}

?>
