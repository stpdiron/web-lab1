

function validateOnSubmit() {
    let yVal = document.getElementById("y-coordinate").value;
    let yNumb = parseFloat(yVal);
    if (isNaN(yNumb) || yNumb < -5 || yNumb > 5) {
        alert("Значение Y должно быть числом от -5 до 5");
        return false;
    }
}